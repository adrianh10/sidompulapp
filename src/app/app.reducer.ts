
import { AppActions, AppActionTypes } from './app.actions';

export interface State {
  isBusy:boolean,
  popupState:any,
}

export const initialState: State = {
  isBusy: false,
  popupState:{
    isVisible: false,
    title: '',
    subtitle: '',
    buttonOneText: '',
    buttonOneCallback:  ()=>{},
    buttonTwoText: '',
    buttonTwoCallback: ()=>{},
    redButton: false,
    type: '',
    onClose:()=>{}
  }
  
};

export const getIsBusy = (state: State) => state.isBusy ;
export const getPopupState = (state: State) => state.popupState; 

export function reducer(state = initialState, action: AppActions): State {
  switch (action.type) {

    case AppActionTypes.SHOW_LOADING:
      return {
        ...state,
        isBusy: true
      };

    case AppActionTypes.HIDE_LOADING:
      return {
        ...state,
        isBusy: false
      };

    case AppActionTypes.SHOW_APP_POPUP:
      return {
        ...state,
        popupState:{
          isVisible: action.payload.isVisible,
          title: action.payload.title,
          subtitle: action.payload.subtitle,
          buttonOneText: action.payload.buttonOneText,
          buttonOneCallback: action.payload.buttonOneAction,
          buttonTwoText: action.payload.buttonTwoText,
          buttonTwoCallback: action.payload.buttonTwoAction,
          redButton: action.payload.redButton,
          type: action.payload.popupType,
          onClose:action.payload.onClose
        }
      };

    case AppActionTypes.HIDE_APP_POPUP:
      return {
        ...state,
        popupState:{
          isVisible: false,
          title: '',
          subtitle: '',
          buttonOneText: '',
          buttonOneCallback:  ()=>{},
          buttonTwoText: '',
          buttonTwoCallback: ()=>{},
          redButton: false,
          type: '',
          onClose:()=>{}
        }
      };

    default:
      return state;
  }
}
