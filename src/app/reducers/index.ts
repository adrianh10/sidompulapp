import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromLogin from '../pages/login/login.reducer';
import * as fromApp from '../app.reducer';
import * as fromNavigator from '../pages/navigator/navigator.reducer';
import * as fromDashboard from '../pages/dashboard/dashboard.reducer';

export interface State {
  login: fromLogin.State;
  app: fromApp.State;
  navigator: fromNavigator.State;
  dashboard: fromDashboard.State;
}

export const reducers: ActionReducerMap<State> = {
  login: fromLogin.reducer,
  app: fromApp.reducer,
  navigator: fromNavigator.reducer,
  dashboard: fromDashboard.reducer,
};


//State Login
export const selectLoginState = createFeatureSelector<fromLogin.State>('login');
export const getIsValid = createSelector(selectLoginState, fromLogin.getIsValid );

//State App
export const selectAppState = createFeatureSelector<fromApp.State>('app');
export const getIsBusy = createSelector(selectAppState, fromApp.getIsBusy);
export const getPopUpState= createSelector(selectAppState, fromApp.getPopupState);

//State Menu
export const selectNavigatorState = createFeatureSelector<fromNavigator.State>('navigator');
export const getMenu = createSelector(selectNavigatorState,fromNavigator.getMenu);

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
