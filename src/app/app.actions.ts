import { PayloadAction } from "./app.modul";

export enum AppActionTypes {
  SHOW_LOADING = "[PAGE] Change isBusy to true",
  HIDE_LOADING = "[PAGE] Change isBusy to false",
  SHOW_APP_POPUP = "[PAGE] Showing App Popup",
  HIDE_APP_POPUP = "[PAGE] Hiding App Popup"
}

export class showLoading extends PayloadAction {
  readonly type = AppActionTypes.SHOW_LOADING;
}

export class hideLoading extends PayloadAction {
  readonly type = AppActionTypes.HIDE_LOADING;
}

export class showAppPopup extends PayloadAction {
  readonly type = AppActionTypes.SHOW_APP_POPUP;
}

export class hideAppPopup extends PayloadAction {
  readonly type = AppActionTypes.HIDE_APP_POPUP;
}

export type AppActions = showLoading | hideLoading | showAppPopup | hideAppPopup;
