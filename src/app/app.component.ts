import { Component, HostListener } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from "@ionic/storage";

import { Store } from "@ngrx/store";
import * as fromState from "./reducers";
import { hideAppPopup } from './app.actions';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  isVisible: boolean=false;
  popupTitle: string='';
  popupSubtitle: string='';
  popupButtonOneText: string='';
  popupButtonTwoText: string='';
  popupRedButton: boolean=false;
  popupType: string='';
  popupButtonOneAction: Function=()=>{};
  popupButtonTwoAction: Function=()=>{};
  public handleOnClose=(ev)=>{

  }

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private storage: Storage,
    private store:Store<fromState.State>
  ) {
    this.initializeApp();
  }

    initializeApp() {
      this.platform.ready().then(() => {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      });

      
      
      this.store.select(fromState.getPopUpState).subscribe((data)=>{
          console.log("Data Popup : ",data)
          this.isVisible=data.isVisible;
          this.popupTitle=data.title;
          this.popupSubtitle=data.subtitle;
          this.popupButtonOneText=data.buttonOneText;
          this.popupButtonOneAction=data.buttonOneCallback;
          this.popupButtonTwoText=data.buttonTwoText;
          this.popupButtonTwoAction=data.buttonTwoCallback;
          this.popupRedButton=data.redButton;
          this.popupType=data.type;
          this.handleOnClose = data.onClose;
      })

      this.storage.get('lang').then((lang: any) => {
        console.log('ac lang', lang);

        if (lang) {
          this.translate.use(lang);
          this.translate.setDefaultLang(lang);
        } else {
          this.storage.set('lang', 'id').then(() => { });
          this.translate.use('id');
          this.translate.setDefaultLang('id');
        }
      });
    }

    @HostListener('document:languageOnChange', ['$event'])

    updateLanguage(event) {
        this.translate.use(event.detail.lang);
        this.translate.setDefaultLang(event.detail.lang);
    }


    public handleOnClose2(ev){
      console.log("EV : ", ev);
      this.handleOnClose(ev);
      this.store.dispatch(new hideAppPopup());
    }

}
