import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
	selector: 'xl-modal',
	templateUrl: './modal.component.html',
	styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
	@Input() isVisible: boolean;
	@Input() title: String = 'Alert Title';
	@Input() subtitle: String = 'Alert Subtitle';
	@Input() buttonOneText: String = 'Button 1';
	@Input() buttonOneCallback: Function;
	@Input() buttonTwoText: String = 'Button 2';
	@Input() buttonTwoCallback: Function;
	@Input() redButton: Boolean = false;
	@Input() type: String = 'oneAction';
	@Output() onClose = new EventEmitter<string>();


	constructor() { }

	ngOnInit() { }

	buttonOnePress() {
		this.isVisible = !(this.isVisible);
		this.onClose.emit('button-one');

		if (this.buttonOneCallback) {
			this.buttonOneCallback();
		}
	}

	buttonTwoPress() {
		this.isVisible = !(this.isVisible);

		if (this.buttonTwoCallback) {
			this.buttonTwoCallback();
		}

		this.onClose.emit('button-two');
	}
}
