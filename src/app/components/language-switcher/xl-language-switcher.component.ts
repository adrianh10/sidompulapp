import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
	templateUrl: './xl-language-switcher.component.html',
	selector: 'xl-language-switcher',
	encapsulation: ViewEncapsulation.None
})
export class XLLanguageSwitcherComponent implements OnInit {
	public activeLang = 'id';

	constructor(private storage: Storage) {
		this.storage.get('lang').then(lang => {
			this.activeLang = lang ? lang : 'id';
		});
	}

	ngOnInit() {
		this.storage.get('lang').then(lang => {
			this.activeLang = lang ? lang : 'id';
		});
	}

	public changeLanguage(lang: any) {
		this.activeLang = lang;
		this.storage.set('lang', lang).then(() => { });
		const event = new CustomEvent('languageOnChange', { detail: { lang: lang }});
		document.dispatchEvent(event);
	}
}
