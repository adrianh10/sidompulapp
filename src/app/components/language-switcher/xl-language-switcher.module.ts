import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { XLButtonModule } from '../xl-button/xl-button.module';
import { XLLanguageSwitcherComponent } from './xl-language-switcher.component';

@NgModule({
	imports: [
		CommonModule,
		XLButtonModule,
		TranslateModule.forChild()
	],
	declarations: [
		XLLanguageSwitcherComponent
	],
	exports: [
		XLLanguageSwitcherComponent,
		TranslateModule
	],
	entryComponents: [XLLanguageSwitcherComponent],
	providers: []
})
export class XLLanguageSwitcherModule { }
