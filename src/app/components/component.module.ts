import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

// import { IconComponentModule } from './icon/icon.module';

// import { NavbarComponent } from './shared/navbar/navbar.component';
// import { HeaderbigComponent } from './shared/headerbig/headerbig.component';
// import { CardComponent } from './shared/card/card.component';
// import { HorizontalscrollComponent } from './shared/horizontalscroll/horizontalscroll.component';
// import { CardlistComponent } from './shared/cardlist/cardlist.component';
// import { BalanceInfoComponent } from './unique/balance-info/balance-info.component';
// import { PackageInfoComponent } from './unique/package-info/package-info.component';
// import { ListviewComponent } from './shared/listview/listview.component';
// import { ProgressbarComponent } from './shared/progressbar/progressbar.component';
// import { HeadingComponent } from './shared/heading/heading.component';
// import { CardleftimageComponent } from './shared/cardleftimage/cardleftimage.component';
// import { ToastComponent } from './shared/toast/toast.component';
// import { ProfileSliderComponent } from './shared/profile-slider/profile-slider.component';
// import { TabsComponent } from './shared/tabs/tabs.component';
// import { profileSliderService } from "./shared/profile-slider/profile-slider.service";
// import { AccountHandle } from "../services/account-handle";
// import { DashboardService } from '../pages/dashboard/dashboard.service';
// import { SearchBarComponent } from './shared/search-bar/search-bar.component';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { ContactPickerComponent } from './shared/contact-picker/contact-picker.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { LottieAnimationViewModule } from "ng-lottie";
// import { Network } from "@ionic-native/network/ngx";
// import { PipesModule } from '../pipes/pipes.module';
// import { NavbarModifierDirective } from "../directives/navbarModifier/navbar-modifier.directive";

@NgModule({
	imports: [
		LottieAnimationViewModule.forRoot(),
		IonicModule,
		CommonModule,
		// FormsModule,
		// ReactiveFormsModule,
		TranslateModule.forChild(),
		// PipesModule
	],
	declarations: [
		// NavbarComponent,
		// HeaderbigComponent,
		// CardComponent,
		// HorizontalscrollComponent,
		// CardlistComponent,
		// BalanceInfoComponent,
		// PackageInfoComponent,
		// ListviewComponent,
		// ProgressbarComponent,
		// HeadingComponent,
		// CardleftimageComponent,
		// ToastComponent,
		// ProfileSliderComponent,
		// TabsComponent,
		// SearchBarComponent,
		// ContactPickerComponent,
		LoadingComponent,
		// NavbarModifierDirective
	],
	exports: [
		TranslateModule,
		// NavbarComponent,
		// HeaderbigComponent,
		// HorizontalscrollComponent,
		// CardComponent,
		// CardlistComponent,
		// CardleftimageComponent,
		// BalanceInfoComponent,
		// PackageInfoComponent,
		// ListviewComponent,
		// ProgressbarComponent,
		// HeadingComponent,
		// ToastComponent,
		// ProfileSliderComponent,
		// TabsComponent,
		// SearchBarComponent,
		// ContactPickerComponent,
		LoadingComponent,
		// NavbarModifierDirective
	],
	entryComponents: []
})

export class SharedComponentModule { 
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: SharedComponentModule,
			providers: [
				TranslateService
				// profileSliderService, AccountHandle, TranslateService, DashboardService, Network
			]
		}
	}
}
