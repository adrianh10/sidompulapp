import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { XLButtonComponent } from './xl-button.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [XLButtonComponent],
	exports: [XLButtonComponent],
	entryComponents: [XLButtonComponent]
})
export class XLButtonModule { }
