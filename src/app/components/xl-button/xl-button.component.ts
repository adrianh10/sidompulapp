import { Component, Input, OnInit } from '@angular/core';

@Component({
	templateUrl: './xl-button.component.html',
	selector: 'xl-button',
	styleUrls: ['./xl-button.component.scss']
})
export class XLButtonComponent implements OnInit {
	@Input() public state: string;
	@Input() public btnColor: string;
	@Input() public btnFull: boolean;
	@Input() public btnSmall: boolean;
	@Input() public disabled: boolean;
	@Input() public styleClass: any;
	@Input() public id: string = 'xl_button';

	public cssClasses: any;

	constructor() { }

	ngOnInit() {
		if (!this.disabled) { this.disabled = false; }

		this.cssClasses = {
			'button--postpaid': this.state === 'postpaid',
			'button--prepaid': this.state === 'prepaid',
			'button--block': this.btnFull,
			'button--auto': this.btnSmall
		};

		if (this.state === 'prepaid') {
			this.cssClasses['button--green'] =  this.btnColor === 'green';
			this.cssClasses['button--orange'] = this.btnColor === 'orange';
		}

		this.cssClasses = { ...this.cssClasses, ...this.styleClass };
	}
}
