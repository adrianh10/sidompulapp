import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { concatMap, switchMap, map, catchError, mergeMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import { LoginActionTypes, LoginActions, CheckingNumber, CheckingNumberFailed, CheckingNumberSuccess } from './login.actions';

import {  LoginService } from "./login.service";
import { showLoading, hideLoading } from 'src/app/app.actions';

import { Router  } from "@angular/router";


@Injectable()
export class LoginEffects {


  @Effect()
  loadLogins$ = this.actions$.pipe(
    ofType(LoginActionTypes.CHECKNUMBER),
    /** An EMPTY observable only emits completion. Replace with your own observable API request */
    switchMap(()=>of(new CheckingNumberSuccess))
    // concatMap((data: CheckingNumber) => this.loginService
    //       .checkValidNumber(data.payload)
    //       .pipe(
    //         map((response: any) => {
    //             return new CheckingNumberSuccess(response.result.opGetRegStatusRs)
    //         }),
    //         catchError(() => of(new CheckingNumberFailed()))
    //       )
    // )
  );

  @Effect()
  startLogin$ = this.actions$.pipe(
    ofType(LoginActionTypes.CHECKNUMBER),
    /** An EMPTY observable only emits completion. Replace with your own observable API request */
    switchMap(()=>of(new showLoading()))
  );

  @Effect()
  finishLoginSuccess$ = this.actions$.pipe(
    ofType(LoginActionTypes.CHECKNUMBER_SUCCESS),
    /** An EMPTY observable only emits completion. Replace with your own observable API request */
    switchMap(()=>of(new hideLoading()))
  );

  @Effect()
  finishLoginFailed$ = this.actions$.pipe(
    ofType(LoginActionTypes.CHECKNUMBER_FAILED),
    /** An EMPTY observable only emits completion. Replace with your own observable API request */
    switchMap(()=>of(new hideLoading()))
  );


  constructor(private actions$: Actions<LoginActions>, private loginService: LoginService, private router:Router) {}

}
