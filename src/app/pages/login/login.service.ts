import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constant } from "../../services/constant.service";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private token: any = new HttpHeaders().set('Authorization', 'Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0');

  constructor(private HttpClient: HttpClient, private constant: Constant) { }

  checkValidNumber(payload: {username: string,imei:string}) {
      return this.HttpClient.get(`${this.constant.loginBasicUrl}/login/otp/registration-status?msisdn=${payload.username}&imei=${payload.imei}`, { headers: this.token })
  }
}
