import { LoginActions, LoginActionTypes } from "./login.actions";

export interface State {
  username: string;
  otp: string;
  isValid:boolean;
}

export const initialState: State = {
  username: undefined,
  otp: undefined,
  isValid:false
};

export const getIsValid = (state: State) => state.isValid;

export function reducer(state = initialState, action: LoginActions): State {
  switch (action.type) {
    case LoginActionTypes.CHECKNUMBER:
      return state;

    case LoginActionTypes.CHECKNUMBER_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isValid: true
      };
   
    case LoginActionTypes.CHECKNUMBER_FAILED:
      return {
        ...state,
        isValid: true
      };

    default:
      return state;
  }
}
