import { PayloadAction } from "../../app.modul";

export enum LoginActionTypes {
  CHECKNUMBER = "[LOGIN PAGE] Checking Number",
  CHECKNUMBER_SUCCESS = "[AUTH API] CHECKNUMBER SUCCESS",
  CHECKNUMBER_FAILED = "[AUTH API] CHECKNUMBER FAILED"
}

export class CheckingNumber extends PayloadAction {
  readonly type = LoginActionTypes.CHECKNUMBER;
}

export class CheckingNumberSuccess extends PayloadAction {
  readonly type = LoginActionTypes.CHECKNUMBER_SUCCESS;
}

export class CheckingNumberFailed extends PayloadAction {
  readonly type = LoginActionTypes.CHECKNUMBER_FAILED;
}


export type LoginActions =
  | CheckingNumber
  | CheckingNumberSuccess
  | CheckingNumberFailed;
