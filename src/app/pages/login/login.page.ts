import { Observable } from 'rxjs';
import { CheckingNumber } from "./login.actions";
import { Component, OnInit } from "@angular/core";

import { Store } from "@ngrx/store";

import * as fromState from "../../reducers";

import { FormGroup,FormBuilder } from '@angular/forms';
import { pluck } from 'rxjs/operators';

import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  constructor(private store: Store<fromState.State>,private fb:FormBuilder, private router:Router) {
   this.store.select(fromState.getIsValid).subscribe((data)=>{
        // console.log('22',data)
        this.isValid=data
   })
  //  this.store.pipe(pluck('app','isBusy')).subscribe((data)=>{
  //    console.log('2',data)
  //    this.isBusy=data
  //  })
   this.store.select(fromState.getIsBusy).subscribe((data)=>{
      // console.log('2',data)
      this.isBusy=data
   })
    // this.isValid$=this.store.pipe(pluck('login', 'isValid'));
    // this.isBusy$=this.store.pipe(pluck('app', 'isBusy'));
  }

  form:FormGroup;

  isBusy:boolean=false;

  isValid:boolean=false;

  public errors: any = {
		username: '',
		password: ''
	};

  ngOnInit() {
    this.buildForm();
  }


  public buildForm(): void {
		const formGroup = {
			username: ['', []],
			password: ['', []]
		};

		this.form = this.fb.group(formGroup);

		this.form.valueChanges.subscribe(values => this.onChangedValues(values));
  }
  
  checkAuthenticationType(event){
    const str: string = event.target.value.trim();
    let phone: string;
    if (str.startsWith('+62')) {
      phone = str.substr(1, str.length - 1);
    } else if (str.startsWith('08')) {
      phone = str.replace('08', '628');
    } else {
      phone = str;
    }
    this.store.dispatch(
      new CheckingNumber({
        username: phone,
        imei: "123456789"
      })
    );
  }

  emailLogin(){
      this.router.navigateByUrl('/navigator/dashboard');
  }


  private onChangedValues(val: any): void {
		// if (!this.form) { return; }

		// const form = this.form;
		// const errors = this.errors;

		// for (const field of Object.keys(this.errors)) {
		// 	const control = form.get(field);
		// 	this.errors[field] = '';

		// 	if (control && control.dirty && !control.valid) {
		// 		for (const key of Object.keys(control.errors)) {
		// 			errors[field] = this.validationMessages[field][key] + ' ';
		// 			this.errors = errors;
		// 		}
		// 	} else if (control && control.invalid && (control.value !== '')) {
		// 		for (const key of Object.keys(control.errors)) {
		// 			errors[field] = this.validationMessages[field][key] + ' ';
		// 			this.errors = errors;
		// 		}
		// 	}
		// }
	}
}
