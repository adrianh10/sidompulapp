import { XLButtonModule } from './../../components/xl-button/xl-button.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';

import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import * as fromLogin from './login.reducer';
import { EffectsModule } from '@ngrx/effects';
import { LoginEffects } from './login.effects';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedComponentModule } from "../../components/component.module";
import { XLLanguageSwitcherModule } from "../../components/language-switcher/xl-language-switcher.module";

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    XLButtonModule,
    SharedComponentModule,
    XLLanguageSwitcherModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    StoreModule.forFeature('login', fromLogin.reducer),
    EffectsModule.forFeature([LoginEffects]),
    ReactiveFormsModule
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
