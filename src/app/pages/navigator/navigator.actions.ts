import { PayloadAction  } from "../../app.modul";

export enum NavigatorActionTypes {
  GetMenuItem = "[NAVIGATOR PAGE] Getting Menu Item",
  GetMenuItemSuccess = "[API] Success Getting Menu",
  GetMenuItemFailed = "[API] Failed Getting Menu",
  OpenSubMenu="[NAVIGATOR PAGE] Open SubMenu Item"
}

export class GetMenuItem extends PayloadAction {
  readonly type = NavigatorActionTypes.GetMenuItem;
}

export class GetMenuItemSuccess extends PayloadAction {
  readonly type = NavigatorActionTypes.GetMenuItemSuccess;
}

export class GetMenuItemFailed extends PayloadAction {
  readonly type = NavigatorActionTypes.GetMenuItemFailed;
}

export class OpenSubMenu extends PayloadAction {
  readonly type = NavigatorActionTypes.OpenSubMenu;
}


export type NavigatorActions = GetMenuItem | GetMenuItemSuccess | GetMenuItemFailed | OpenSubMenu;
