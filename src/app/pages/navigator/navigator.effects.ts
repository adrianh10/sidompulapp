import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { concatMap, switchMap,map, catchError } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import { NavigatorActionTypes, NavigatorActions, GetMenuItemSuccess, GetMenuItemFailed } from './navigator.actions';
import { NavigatorService } from "./navigator.service";


@Injectable()
export class NavigatorEffects {
  
  @Effect()
  loadNavigators$ = this.actions$.pipe(
    ofType(NavigatorActionTypes.GetMenuItem),
    /** An EMPTY observable only emits completion. Replace with your own observable API request */
    switchMap(() => this.service.getMenu()
              .pipe(
                map((data:any)=>new GetMenuItemSuccess(data.data)),
                catchError(()=>of(new GetMenuItemFailed()))
              )
    )
  );


  constructor(private actions$: Actions<NavigatorActions>, private service : NavigatorService) {}

}
