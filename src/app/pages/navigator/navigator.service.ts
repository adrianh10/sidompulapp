import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class NavigatorService {

  constructor(private http: HttpClient) { }

  getMenu(){
    return this.http.get(`assets/dataset/navbar_test.json`)
  }
}
