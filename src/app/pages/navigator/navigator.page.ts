import { GetMenuItem, OpenSubMenu } from './navigator.actions';
import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from "@angular/router";
import {Store} from '@ngrx/store'
import * as fromNavigator from '../../reducers'

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.page.html',
  styleUrls: ['./navigator.page.scss'],
})
export class NavigatorPage implements OnInit {

  pages: Array<any> = [];
  selectedPath: string = '';

  constructor(private store: Store<fromNavigator.State>,private router:Router) { 
      this.router.events.subscribe((event:RouterEvent)=>{
          this.selectedPath = event.url;
      });
  }

  ngOnInit(){
    this.store.dispatch(new GetMenuItem());
    this.store.select(fromNavigator.getMenu).subscribe(data => {
      this.pages = [...data]
    });
  }

  openSubMenu(index) {
    this.store.dispatch(new OpenSubMenu(index))
  }

  ionViewWillEnter() {
    this.selectedPath=this.router.url
  }


}
