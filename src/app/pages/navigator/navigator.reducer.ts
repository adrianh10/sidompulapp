
import { NavigatorActions, NavigatorActionTypes } from './navigator.actions';

export interface State {
  menuItem:Array<any>
}

export const initialState: State = {
  menuItem:[]
};

export const getMenu = (state: State) => state.menuItem;

export function reducer(state = initialState, action: NavigatorActions): State {
  switch (action.type) {

    case NavigatorActionTypes.GetMenuItemSuccess:
      return {
        ...state,
        menuItem:[...action.payload]
      };

    case NavigatorActionTypes.OpenSubMenu:
      let newMenuItem=[...state.menuItem].slice(0,action.payload)
      let leftArray=[...state.menuItem].slice(action.payload+1,state.menuItem.length)
      let tempItem={
            "title":state.menuItem[action.payload].title,
            "open": !state.menuItem[action.payload].open,
            "childern": state.menuItem[action.payload].childern
      }
      newMenuItem=[
        ...newMenuItem,
        tempItem,
        ...leftArray
      ]
      return {
        ...state,
        menuItem:newMenuItem
      };

    default:
      return state;
  }
}
