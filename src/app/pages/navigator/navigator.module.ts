import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NavigatorPage } from './navigator.page';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromNavigator from './navigator.reducer';
import { NavigatorEffects } from './navigator.effects';

const routes: Routes = [
  {
    path: '',
    redirectTo:'navigator/dashboard',
    pathMatch:'full'
  },
  {
    path: '',
    component:NavigatorPage,
    children:[
      {
        path:'dashboard',
        loadChildren:'../dashboard/dashboard.module#DashboardPageModule' 
      },
      {
        path:'profile',
        loadChildren:'../profile/profile.module#ProfilePageModule'
      },
      {
        path:'inbox',
        loadChildren:'../inbox/inbox.module#InboxPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('navigator', fromNavigator.reducer),
    EffectsModule.forFeature([NavigatorEffects])
  ],
  declarations: [NavigatorPage]
})
export class NavigatorPageModule {}
