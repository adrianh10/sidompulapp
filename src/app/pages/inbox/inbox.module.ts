import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo:'inbox/notification',
    pathMatch:'full'
  },
  {
    path: '',
    children:[
      {
        path:'notification',
        loadChildren:'./pages/notification/notification.module#NotificationPageModule' 
      },
      {
        path:'transaction',
        loadChildren:'./pages/transaction/transaction.module#TransactionPageModule'
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class InboxPageModule {}