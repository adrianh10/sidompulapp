import { Component, OnInit } from '@angular/core';

import { Store } from "@ngrx/store";
import * as fromState from "../../reducers"
import { showAppPopup } from 'src/app/app.actions';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor(private store:Store<fromState.State>) { }

  ngOnInit() {
  }

 

  testPopup(){
      this.store.dispatch(new showAppPopup({
        isVisible:true,
        title:"Test",
        subtitle: "Test Sub" ,
        buttonOneText: "Test Button 1",
        buttonOneAction:()=>{
          console.log("Button 1")
        },
        buttonTwoText: "Test Button 2",
        buttonTwoAction: ()=>{
          console.log("Button 2")
        },
        redButton: true,
        popupType:"twoActions",
        onClose:(ev)=>{
          console.log("Custom Function Dasboard OnClose")
        }
      }));
  }

}
