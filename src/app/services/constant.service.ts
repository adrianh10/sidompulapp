import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable()
export class Constant {
	constructor(private storage: Storage) { }

	public basicToken: any = new HttpHeaders().set('Authorization', 'Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0');
	public loginBasicUrl: String = 'https://login-controller-service-stagging.apps.pcf.kb.dp.xl.co.id/v1';
	public accountBasicUrl: String = 'https://account-service-test.apps.pcf.kb.dp.xl.co.id/v1';
	public otpBasicUrl: String = 'https://otp-service-test.apps.pcf.kb.dp.xl.co.id/v1';
	public subscriberInfoBasicUrl: String = 'https://subscriber-info-service-test.apps.pcf.kb.dp.xl.co.id/v1';
	public storefrontBasicUrl: String = 'https://storefront-service-test.apps.pcf.kb.dp.xl.co.id/v1';
	public billingBaseUrl: String = 'https://billing-service-test.apps.pcf.kb.dp.xl.co.id';
	public billingBasicUrl: String = 'https://billing-service-test.apps.pcf.kb.dp.xl.co.id/v1';
	public paymentBasicUrl: String = 'https://payment-service-test.apps.pcf.kb.dp.xl.co.id/v1';
	public autologinUrl: String = 'http://apigw.xl.co.id/public/access';
	public subscriptionBasicUrl: String = 'https://subscription-service-test.apps.pcf.kb.dp.xl.co.id/v1';
	public pgURLAplpha: String = 'https://myxl.alphabill.id/beliXL';
	public contentBasicUrl: String = 'https://content-service-test.apps.pcf.kb.dp.xl.co.id';
	public shareBalanceBasicUrl: String = 'https://share-balance-service-test.apps.pcf.kb.dp.xl.co.id';
	public xlGoUrl: string = 'http://xl.go/';
	public roamingQuestionId: any = {
		prepaid: '41726',
		postpaid: '94',
		xlGo: '41756'
	};
	public tncShareBalanceUrl: string = 'https://www.xl.co.id/id/mobile/vas/bagi-pulsa';
	public id: string = 'EB3BBFC3B18ABCD7DBD29B2B82AFG';


	async authToken() {
		const promise = new Promise((resolve) => {
			this.storage.get('me').then(val => {
				resolve(JSON.parse(val).accessToken);
			});
		});

		const result = await promise;

		return result;
	}
}
